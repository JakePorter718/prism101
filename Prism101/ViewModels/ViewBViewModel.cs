﻿using Prism.Events;
using Prism.Mvvm;
using Prism101.Events;

namespace Prism101.ViewModels
{
    public class ViewBViewModel : BindableBase
    {
        private string _message = "ViewB";
        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }

        public ViewBViewModel(IEventAggregator eventAggregator)
        {
            eventAggregator.GetEvent<UpdateEvent>().Subscribe(Update);
        }

        public void Update(string obj)
        {
            Message = obj;
        }
    }
}
