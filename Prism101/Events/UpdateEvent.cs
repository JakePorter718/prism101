﻿using Prism.Events;

namespace Prism101.Events
{
    public class UpdateEvent : PubSubEvent<string>
    {
    }
}
